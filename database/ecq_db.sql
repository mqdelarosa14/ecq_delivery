-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2020 at 09:17 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecq`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `b_status` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_name`, `description`, `b_status`, `created_at`, `updated_at`) VALUES
(1, 'Head Office', 'This is the head office', 'available', '2020-05-04 14:46:41', '2020-05-04 14:46:41'),
(2, 'EDSA Shang Ri La', 'This is edsa shangrila branch', 'available', '2020-05-04 14:46:41', '2020-05-04 14:46:41'),
(3, 'Via Mare Alabang', 'This is viamare alabang', 'available', '2020-05-04 15:05:57', '2020-05-04 15:05:57');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `hidden` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `hidden`, `created_at`, `updated_at`) VALUES
(1, 'beverages', 0, '2020-04-30 19:36:03', '2020-04-30 19:36:03'),
(2, 'breakfast', 0, '2020-04-30 19:36:03', '2020-04-30 19:36:03'),
(3, 'lunch2', 0, '2020-05-01 00:20:55', '2020-05-01 00:20:55');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `prod_name` varchar(200) NOT NULL,
  `prod_code` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `menu_status` varchar(50) NOT NULL DEFAULT 'available',
  `stock` int(100) NOT NULL,
  `hidden` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `category_id`, `prod_name`, `prod_code`, `price`, `menu_status`, `stock`, `hidden`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pepsi', 'plsi1', 50, 'available', 100, 0, '2020-04-30 19:35:09', '2020-04-30 19:35:27'),
(2, 2, 'pancake', 'pnck', 80, 'out of stock', 100, 0, '2020-04-30 19:35:09', '2020-04-30 19:35:27'),
(3, 2, 'pandesal', 'pndsl', 50, 'available', 100, 0, '2020-04-30 19:35:09', '2020-04-30 19:35:27'),
(4, 2, 'Patatas', 'ptts', 140.5, 'available', 0, 0, '2020-05-01 00:10:06', '2020-05-01 00:10:06'),
(5, 1, 'Coke', 'ccola', 45.55, 'available', 0, 0, '2020-05-01 00:20:03', '2020-05-01 00:20:03'),
(6, 3, 'Premium Ginataan', 'prmgntn', 145.95, 'out of stock', 0, 0, '2020-05-03 18:38:02', '2020-05-03 18:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile_no` int(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `total` double NOT NULL,
  `status` int(100) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `email`, `mobile_no`, `address`, `firstname`, `lastname`, `notes`, `total`, `status`, `created_at`, `updated_at`) VALUES
(1, 'jwick@gmail.com', 0, '1989 Jogn wCik Street', 'John', 'Wick', 'Test', 750, 2, '2020-04-30 20:19:48', '2020-04-30 20:19:48'),
(2, 'mickojel.delarosa@gmail.com', 0, '1989 Jogn wCik Street', 'Micko Jel', 'Dela Rosa', 'test', 160, 0, '2020-04-30 20:53:02', '2020-04-30 20:53:02'),
(3, 'mickojel.delarosa@gmail.com', 0, '1989 Jogn wCik Street', 'Micko Jel', 'Dela Rosa', 'test', 400, 0, '2020-04-30 20:54:09', '2020-04-30 20:54:09'),
(4, 'mickojel.delarosa@gmail.com', 0, '1989 Jogn wCik Street', 'Micko Jel', 'Dela Rosa', 'test', 100, 0, '2020-04-30 20:55:18', '2020-04-30 20:55:18'),
(5, 'mickojel.delarosa@gmail.com', 0, '1989 Jogn wCik Street', 'Micko Jel', 'Dela Rosa', 'test', 400, 0, '2020-04-30 20:57:18', '2020-04-30 20:57:18'),
(6, 'mickojel.delarosa@gmail.com', 0, '1989 Jogn wCik Street', 'Micko Jel', 'Dela Rosa', 'test', 100, 0, '2020-04-30 20:58:38', '2020-04-30 20:58:38'),
(7, 'mickojel.delarosa@gmail.com', 0, '1989 Jogn wCik Street', 'Micko Jel', 'Dela Rosa', 'test', 250, 0, '2020-04-30 23:38:09', '2020-04-30 23:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `od_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`od_id`, `order_id`, `menu_id`, `qty`) VALUES
(4, 1, 2, 5),
(5, 1, 3, 5),
(6, 1, 1, 2),
(7, 2, 2, 2),
(8, 3, 2, 5),
(9, 4, 1, 2),
(10, 5, 2, 5),
(11, 6, 3, 2),
(12, 7, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `order_id`, `content`, `created_at`) VALUES
(1, 1, 'Order was received', '2020-05-03 19:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `mobile_no` bigint(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `role`, `fname`, `lname`, `mobile_no`, `email`, `password`, `branch_id`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'micko jel', 'dela rosa', 9092969518, 'admin@viamare.com.ph', '123', 0, '2020-05-03 20:14:29', '2020-05-03 20:14:29'),
(2, 'administrator', 'micko jel', 'dela rosa', 123, 'mickojel.delarosa@viamare.com.ph', '123', 0, '2020-05-03 20:14:44', '2020-05-03 20:14:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`od_id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `od_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`);

--
-- Constraints for table `order_status`
--
ALTER TABLE `order_status`
  ADD CONSTRAINT `order_status_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
