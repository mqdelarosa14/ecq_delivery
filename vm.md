

## Backlog

- [ ] Integrate Peso Pay API
- [ ] Upload QR Code for PayMaya
- [ ] Upload QR Code for Gcash
- [ ] Add Branch Availability Before Ordering


## To do

- [ ] Add Email notification of order status
- [ ] Add Category Module
- [ ] Create Order Page
- [ ] Create Order Management Page


## Test

- [x] Create admin page


## Done

- [ ] Create Database
- [ ] Add Login For Administrator Page
- [ ] Add Menu Out of Stock Feature
- [ ] Add Menu Module
- [ ] Create email receipt
- [ ] Create checkout page
