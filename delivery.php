<?php include 'header.php' ?>

<?php 
//available branch query
$branch_sql="SELECT * FROM branch WHERE b_status='available'";
$branch_res=$connection->query($branch_sql);
//end avaiable branch query

//swtich case
$db_handle = new DBController();
if(!empty($_GET["action"])) {
    switch($_GET["action"]) {
        case "add":
            if(!empty($_POST["quantity"])) {
                $productByCode = $db_handle->runQuery("SELECT * FROM menu LEFT JOIN category ON category.category_id=menu.category_id WHERE menu.hidden=0 AND prod_code='" . $_GET["prod_code"] . "' ORDER BY category.category_name ASC, menu.prod_name ASC");



                $itemArray = array($productByCode[0]["prod_code"]=>array('prod_code'=>$productByCode[0]["prod_code"],'menu_id'=>$productByCode[0]["menu_id"],'prod_name'=>$productByCode[0]["prod_name"], 'category_name'=>$productByCode[0]["category_name"], 'quantity'=>$_POST["quantity"], 'price'=>$productByCode[0]["price"]));
                
                if(!empty($_SESSION["cart_item"])) {
                    if(in_array($productByCode[0]["prod_code"],array_keys($_SESSION["cart_item"]))) {
                        foreach($_SESSION["cart_item"] as $k => $v) {
                                if($productByCode[0]["prod_code"] == $k) {
                                    if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                                        $_SESSION["cart_item"][$k]["quantity"] = 0;
                                    }
                                    $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                                    
                                }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
            }
        break;
        case "remove":
            if(!empty($_SESSION["cart_item"])) {
                foreach($_SESSION["cart_item"] as $k => $v) {
                        if($_GET["prod_code"] == $k)
                            unset($_SESSION["cart_item"][$k]);				
                        if(empty($_SESSION["cart_item"]))
                            unset($_SESSION["cart_item"]);
                }
            }
        break;
        case "checkout":
          header('location:checkout.php');

        break;
        case "empty":
            unset($_SESSION["cart_item"]);
        break;	
    }
    }
?>


<div class="jumbotron">
  <h1 class="display-4">Hello, Customer!</h1>
  <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
  <hr class="my-4">
  <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
  <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
</div>


<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Outlet Ordering Page 
          </div>
          <div class="card-body">




            <div class="table-responsive">
              <!-- <button class="btn btn-primary btn-block btn-sm mb-3" data-toggle="modal" data-target="#add_order">Create Order</button> -->

		<table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
			<thead>
				<th>Product Name</th>
				<th>Category</th>
				<th>Price</th>
        <th>Quantity</th>
        <th>Action</th>
			</thead>
			<tbody>
      <?php
	$product_array = $db_handle->runQuery("SELECT * FROM menu LEFT JOIN category ON category.category_id=menu.category_id WHERE menu.hidden=0 AND menu.menu_status='available' ORDER BY category.category_name ASC, menu.prod_name ASC");
	if (!empty($product_array)) { 
		foreach($product_array as $key=>$value){
	?>
						<tr>
            <form method="post" action="delivery.php?action=add&prod_code=<?php echo $product_array[$key]["prod_code"]; ?>">
							<td><?php echo $product_array[$key]["prod_name"]; ?></td>
							<td><?php echo $product_array[$key]["category_name"]; ?></td>
							<td class="text-right">&#8369; <?php echo $product_array[$key]["price"]; ?></td>
              <td><input type="number" class="form-control" name="quantity" /></td>
              <td><input type="submit" value="Add to Cart" class="btn btn-primary" /></td>
            </form>
            </tr>
            <?php
		}
	}
	?>
			</tbody>
		</table>
		
			<div class="row">
            <div class="col-md-3">
              <input type="hidden" name="outlet" class="form-control" value="<?php echo ucfirst($_SESSION['branch']); ?>">
            </div>
          </div>
          
          
      </div>
    </div>
    <div class="card-footer small text-muted"></div>
    
  </div>

  

  <!-- TEST MODAL HERE  -->

<!-- Order Details Modal-->
<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order</h5>
      </div>
      <div class="modal-body" id="printSection">


<!-- <a class="btn btn-success" href="test.php?action=checkout">Checkout</a> -->
<!-- <a class="btn btn-danger" href="test.php?action=empty">Empty Cart</a> -->
<?php
if(isset($_SESSION["cart_item"])){
    $total_quantity = 0;
    $total_price = 0;
?>	

<table class="table table-striped table-bordered table-hover mt-3" >
<tbody>
<tr>
<th >Name</th>
<th >Category</th>
<th >Quantity</th>
<th >Product Price</th>
<th >Total Price</th>
<th >Remove</th>
</tr>	
<?php		
    foreach ($_SESSION["cart_item"] as $item){
        $item_price = $item["quantity"]*$item["price"];
		?>
				<tr>
				<td><?php echo $item["prod_name"]; ?></td>
				<td><?php echo $item["category_name"]; ?></td>
				<td style="text-align:right;"><?php echo $item["quantity"]; ?></td>
				<td  style="text-align:right;">&#8369; <!-- PESO SIGN --><?php echo $item["price"]; ?></td>
				<td  style="text-align:right;">&#8369; <!-- PESO SIGN --><?php echo number_format($item_price,2); ?></td>
				<td style="text-align:center;"><a href="delivery.php?action=remove&prod_code=<?php echo $item["prod_code"]; ?>" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a></td>
				</tr>
				<?php
				$total_quantity += $item["quantity"];
				$total_price += ($item["price"]*$item["quantity"]);
		}
		?>

<tr>
<td colspan="2" align="right">Grand Total:</td>
<td align="right"><?php echo $total_quantity; ?></td>
<td align="right" colspan="2"><strong> &#8369; <!-- PESO SIGN --> <?php echo number_format($total_price, 2); ?></strong></td>
<td></td>
</tr>
</tbody>
</table>		
  <?php
} else {
?>
<div class="no-records">Your Cart is Empty</div>
<?php 
}
?>


			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-arrow-left">&nbsp;</i>  Go back </button>
      <a class="btn btn-warning" href="delivery.php?action=empty">Empty Cart</a>
			<a class="btn btn-success" href="delivery.php?action=checkout">Checkout</a>
      	</form>
      <!-- <button type="submit" name="add" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span> Add</button> -->
      </div>
    </div>
  </div>
</div>


<button data-toggle="modal"  data-target="#preview" class="btn btn-primary btn-sm float"><i class="fas fa-shopping-cart">&nbsp;</i>Cart</button>



<!-- Modal -->
<div class="modal fade" id="notif_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Via Mare Branches That Are Open Today</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        <?php while($b_row=$branch_res->fetch_array()){ ?>
          <div class="col-sm">
            <div class="card" style="width: 18rem;">
              <img src="media/vm.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title"><?php echo $b_row['branch_name'] ?></h5>
                <p class="card-text"><?php echo $b_row['description'] ?></p>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>


<?php include 'footer.php' ?>
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
$('#notif_modal').modal('show');
</script>

