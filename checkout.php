<?php 
include 'header.php';
$db_handle = new DBController();

if(isset($_POST['place_order']))
{
  if(!empty($_POST["notes"])) {
            if(!empty($_SESSION["cart_item"])) {

                            $email=$_POST['email'];
                            $mobile_no=$_POST['mobile_no'];
                            $fname=$_POST['fname'];
                            $lname=$_POST['lname'];
                            $address=$_POST['address'];
                            $notes = $_POST['notes'];
                            $sql="INSERT INTO orders (email, mobile_no, firstname, lastname, address, notes) VALUES ('$email',$mobile_no, '$fname', '$lname', '$address', '$notes')";
                            $connection->query($sql);
                            $pid=$connection->insert_id;

                            $total=0;

                foreach($_SESSION["cart_item"] as $k => $v) {


                            $sql="SELECT * FROM menu WHERE prod_code='".$v["prod_code"]."'";
                            $query=$connection->query($sql);
                            $row=$query->fetch_array();
                            

                            $subt=$row['price']*$v["quantity"]; 
                            $total+=$subt;


                            $db_handle->storeProd("INSERT INTO order_details (order_id, menu_id, qty) VALUES ('$pid', '".$v["menu_id"]."', '".$v["quantity"]."')");

                } //end foreach
                            $sql="UPDATE orders SET total='$total', status='pending' WHERE order_id='$pid'";
                            $connection->query($sql);
                            
                            // send notification
                            // $branch = $_SESSION['branch'];
                            // $message = ucfirst($branch).' sent you an order!';
                            // $notif_sql = "INSERT INTO comments(send_to, comment_subject, comment_text)VALUES ('commi','$message', '')";
                            // $connection->query($notif_sql);
                            $to = $email;
                            $subject = "Order Number 000000".$pid;
                            
                            $message = "
                            <html>
                            <head>
                            <title>HTML email</title>
                            <style>
                            table, th, td {
                              border: 1px solid black;
                              border-collapse: collapse;
                            }
                            th, td {
                              padding: 5px;
                              text-align: left;
                            }
                            </style>
                            
                            </head>
                            <body>
                            <table style='width:100%' >
                            <tr>
                            <th >Name</th>
                            <th >Category</th>
                            <th >Quantity</th>
                            <th >Product Price</th>
                            <th >Total Price</th>
                            </tr>	";

                                foreach ($_SESSION['cart_item'] as $item){
                                    $item_price = $item['quantity']*$item['price'];

                            $message2 .= "
                                            <tr>
                                            <td>".$item['prod_name']."</td>
                                            <td>".$item['category_name']."</td>
                                            <td>".$item['quantity']."</td>
                                            <td>".$item['price']."</td>
                                            <td>".number_format($item_price,2)."</td>
                                            </tr>
                                            ";
                                            
                                            $total_quantity += $item['quantity'];
                                            $total_price += ($item['price']*$item['quantity']);
                                    }
                                    
                            $message3 = "
                            <tr>
                            <td colspan='2'><b>Grand Total:<b></td>
                            <td >".$total_quantity."</td>
                            <td style='padding-left: 350px;' colspan='2'><strong> &#8369; ".number_format($total_price, 2)." </strong></td>
                            </tr>
                            </table>	
                            <script src='https://code.jquery.com/jquery-3.4.1.slim.min.js' integrity='sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n' crossorigin='anonymous'></script>
                            <script src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js' integrity='sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo' crossorigin='anonymous'></script>
                            <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js' integrity='sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6' crossorigin='anonymous'></script>
                            </body>
                            </html>
                            ";
                            
                            // Always set content-type when sending HTML email
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                            
                            // More headers
                            $headers .= 'From: <vm_team@viamare.com.ph>' . "\r\n";
                            
                            mail($to,$subject,$message.$message2.$message3,$headers);

                            header('location:delivery.php');	
            }
            unset($_SESSION["cart_item"]);
          } else {
            header('location:checkout.php');	
          }
}

//query for product
//$product_sql="SELECT * FROM menu WHERE hidden=0 ORDER BY menu_id ASC";
$product_sql="SELECT * FROM menu LEFT JOIN category ON category.category_id=menu.category_id WHERE menu.hidden=0 ORDER BY category.category_name ASC, menu.prod_name ASC";
$product_result=$connection->query($product_sql);



if(!empty($_GET["action"])) {
    switch($_GET["action"]) {
        case "add":
            if(!empty($_POST["quantity"])) {
                $productByCode = $db_handle->runQuery("SELECT * FROM menu LEFT JOIN category ON category.category_id=menu.category_id WHERE menu.hidden=0 AND prod_code='" . $_GET["prod_code"] . "' ORDER BY category.category_name ASC, menu.prod_name ASC");



                $itemArray = array($productByCode[0]["prod_code"]=>array('prod_code'=>$productByCode[0]["prod_code"],'menu_id'=>$productByCode[0]["menu_id"],'prod_name'=>$productByCode[0]["prod_name"], 'category_name'=>$productByCode[0]["category_name"], 'quantity'=>$_POST["quantity"], 'price'=>$productByCode[0]["price"]));
                
                if(!empty($_SESSION["cart_item"])) {
                    if(in_array($productByCode[0]["prod_code"],array_keys($_SESSION["cart_item"]))) {
                        foreach($_SESSION["cart_item"] as $k => $v) {
                                if($productByCode[0]["prod_code"] == $k) {
                                    if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                                        $_SESSION["cart_item"][$k]["quantity"] = 0;
                                    }
                                    $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                                    
                                }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
            }
        break;
        case "remove":
            if(!empty($_SESSION["cart_item"])) {
                foreach($_SESSION["cart_item"] as $k => $v) {
                        if($_GET["prod_code"] == $k)
                            unset($_SESSION["cart_item"][$k]);				
                        if(empty($_SESSION["cart_item"]))
                            unset($_SESSION["cart_item"]);
                }
            }
        break;
        case "checkout":
          header('location:checkout.php');
        break;
        case "empty":
            unset($_SESSION["cart_item"]);
        break;	
    }
    }
?>


        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Checkout Page 
          </div>
            <div class="card-body">


<form method="post">

  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" required>
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Mobile Number</label>
    <input type="number" class="form-control" name="mobile_no" aria-describedby="emailHelp" required>
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">First Name</label>
    <input type="text" class="form-control" name="fname" aria-describedby="emailHelp" required>
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Last Name</label>
    <input type="text" class="form-control" name="lname" aria-describedby="emailHelp" required>
  </div>

  <!-- <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Pick up from store ?</label>
  </div> -->

  <div class="form-group">
    <label for="exampleInputEmail1">Delivery Address</label>
    <input type="text" class="form-control" name="address" aria-describedby="emailHelp" required>
  </div>

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Additional Instructions </label>
    <textarea class="form-control" id="exampleFormControlTextarea1" name="notes" rows="3" placeholder="Optional"></textarea>
  </div>


<?php
if(isset($_SESSION["cart_item"])){
    $total_quantity = 0;
    $total_price = 0;
?>	

<table class="table table-striped table-bordered table-hover mt-3" >
<tbody>
<tr>
<th >Name</th>
<th >Category</th>
<th >Quantity</th>
<th >Product Price</th>
<th >Total Price</th>
</tr>	
<?php		
    foreach ($_SESSION["cart_item"] as $item){
        $item_price = $item["quantity"]*$item["price"];
		?>
				<tr>
				<td><?php echo $item["prod_name"]; ?></td>
				<td><?php echo $item["category_name"]; ?></td>
				<td style="text-align:right;"><?php echo $item["quantity"]; ?></td>
				<td  style="text-align:right;">&#8369; <!-- PESO SIGN --><?php echo $item["price"]; ?></td>
				<td  style="text-align:right;">&#8369; <!-- PESO SIGN --><?php echo number_format($item_price,2); ?></td>
				</tr>
				<?php
				$total_quantity += $item["quantity"];
				$total_price += ($item["price"]*$item["quantity"]);
		}
		?>

<tr>
<td colspan="2" align="right">Grand Total:</td>
<td align="right"><?php echo $total_quantity; ?></td>
<td align="right" colspan="2"><strong> &#8369; <!-- PESO SIGN --> <?php echo number_format($total_price, 2); ?></strong></td>
</tr>
</tbody>
</table>		
  <?php
} else {
?>
<div class="alert alert-danger d-flex justify-content-center">Your Cart is Empty</div>
<?php 
}
?>


</div>
<h2 class="d-flex justify-content-center">Scan the QR Code of your preferred payment.</h2>
<div class="container d-flex justify-content-center mb-2"><span class="badge badge-pill badge-info">We'll verify your payment and you'll receive a copy of your order to your email.</span></div>
  <div class="row mt-3">
    <div class="col">
        <h2 class="d-flex justify-content-center">PAYMAYA</h2>
        <div class="text-center">
            <img src="media/qr.png" class="img-thumbnail" width="200" alt="QR CODE FOR PAYMAYA">
        </div>
    </div>
    <div class="col">
        <h2 class="d-flex justify-content-center">GCASH</h2>
        <div class="text-center">
            <img src="media/qr.png" class="img-thumbnail" width="200" alt="QR CODE FOR GCASH">
        </div>
    </div>
  </div>


			
			<input type="submit" class="btn btn-success mt-2" value="Place order" name="place_order" <?php if(empty($_SESSION["cart_item"])){ ?> disabled <?php } ?>>
            <a class="btn btn-danger mt-2" href="delivery.php">Go back</a>
      </form>
            </div>
    <div class="card-footer small text-muted"></div>


  

  <?php include 'footer.php' ?>