<?php 
include 'header.php';

//query for product
$product_sql="SELECT * FROM menu LEFT JOIN category ON category.category_id=menu.category_id WHERE menu.hidden=0 ORDER BY menu.category_id ASC, prod_name ASC";
$product_result=$connection->query($product_sql);


$order_sql="SELECT * FROM orders
            ORDER BY email DESC";



$order_result=$connection->query($order_sql);
$number_of_orders = mysqli_num_rows($order_result);


?>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Orders List
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <!-- <button class="btn btn-primary btn-block btn-sm mb-3" data-toggle="modal" data-target="#add_order">Create Order</button> -->
              <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Email Address</th>
                    <th>Notes</th>
                    <th>Status</th>
                    <th>Order Submitted</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Email Address</th>
                    <th>Notes</th>
                    <th>Status</th>
                    <th>Order Submitted</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                <!-- START PHP SCRIPT CATERGORY -->
                <?php 
                while($row=$order_result->fetch_array()){
                ?>
                  <tr>
                    <td><?php echo ucfirst($row['email']); ?></td>
                    
                    <td><?php echo $row['notes']; ?></td>
                    <td <?php if($row['status'] == 3) { ?>  data-toggle="tooltip" data-placement="top" title="Order is on its way. Awaiting outlet's confirmation." <?php } ?>>
               
                    <?php if($row['status'] != 4) {
                      
                      // 0 - Pending 
                      // 1 - Received
                      // 2 - Processing
                      // 3 - In Transit
                      // 4 - Delivered

                      //condition for status bar percentage
                      $progress_percentage = 0; // value of progress bar
                      $percentage_text = ''; // value of progress bar
                      if($row['status'] == 1){
                        $progress_percentage = 25;
                        $percentage_text = 'Received'; 
                      } elseif($row['status'] == 2){
                        $progress_percentage = 50;
                        $percentage_text = 'Processing'; 
                      } elseif($row['status'] == 3){
                        $progress_percentage = 75;
                        $percentage_text = 'In Transit'; 
                      } elseif($row['status'] == 0){
                        $progress_percentage = 0;
                        $percentage_text = 'Pending'; 
                      } else {
                        $progress_percentage = 100;
                        $percentage_text = 'Delivered'; 
                      }

                    ?>
                    <div class="progress">
                      <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: <?php echo $progress_percentage;?>%"aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>

                    <?php 
                    }

                    if($percentage_text == 'Pending'){
                      echo "<label class='badge badge-warning '>".$percentage_text."</label>"; 
                    } elseif($percentage_text == 'Received') {
                      echo "<label class='badge badge-info '>".$percentage_text."</label>"; 
                    } elseif($percentage_text == 'In Transit') {
                      echo "<label class='badge badge-primary '>".$percentage_text."</label>"; 
                    } elseif($percentage_text == 'Processing') {
                      echo "<label class='badge badge-primary '>".$percentage_text."</label>"; 
                    } elseif($percentage_text == 'Delivered') {
                      echo "<label class='badge badge-success '>".$percentage_text."</label>"; 
                    }
                    ?>

                    </td>
                    <td>
                    <?php 
                    $date_created = date_create($row['created_at']); 
                    echo date_format($date_created, "Y-m-d H:i:s");
                    ?>
                    </td>
                    <td>
                    <!-- <a href="#" data-toggle="modal" onclick="copyId()" data-target="#order_details<?php echo $row['order_id'];?>" class="btn btn-success btn-sm"><i class="far fa-calendar">&nbsp;</i>Details <?php echo $row['order_id'];?></a>  -->
                    <a href="order_detail_view.php?order_id=<?php echo $row['order_id'];?>" class="btn btn-success btn-sm"><i class="far fa-calendar">&nbsp;</i>Details</a>
                    </td>

                  </tr>
                <?php } ?>

                
                <!-- END PHP SCRIPT CATEGORY -->
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted"></div>
        </div>


<?php include 'footer.php' ?>
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>


