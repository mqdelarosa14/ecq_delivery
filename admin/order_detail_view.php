<?php 
include 'header.php';

if(isset($_GET['order_id']))
{
    $ord_id = $_GET['order_id'];
    $o_details_sql="SELECT menu.prod_name, menu.price, order_details.qty
                    FROM order_details 
                    LEFT JOIN menu ON menu.menu_id=order_details.menu_id
                    WHERE order_id=$ord_id 
                    ORDER BY prod_name ASC";

    $o_details_result=$connection->query($o_details_sql);

    //query for percentage of status
    $percentage_sql="SELECT status FROM orders
                    WHERE order_id=$ord_id";

    $per_res=$connection->query($percentage_sql);


    //query for status
    $order_status_sql="SELECT * FROM orders
                    JOIN order_status ON  order_status.order_id = orders.order_id
                    WHERE orders.order_id=$ord_id
                    ORDER BY order_status.created_at DESC";

    $order_status_res=$connection->query($order_status_sql);
}

if(isset($_POST['send'])){
  $email = $_POST['email'];
  $msg = $_POST['msg'];
  $subject = "Order Number 000000".$ord_id;

  // Always set content-type when sending HTML email
  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

  $headers .= 'From: <vm_team@viamare.com.ph>' . "\r\n";
  
  mail($email,$subject,$msg,$headers);

  header('location:order_detail_view.php?order_id='.$ord_id);	

}

$or_id = $_GET['order_id'];


    //query for orders
    
    $order_sql="SELECT * FROM orders
                WHERE orders.order_id = '$or_id'
                ORDER BY orders.created_at DESC";
    
    $order_result=$connection->query($order_sql);

    //query for status

    $action_order_sql="SELECT * FROM orders
            WHERE orders.order_id = '$or_id'
            ORDER BY orders.created_at DESC";

    $actord_res=$connection->query($action_order_sql);




if(!empty($_GET["action"])) {
  switch($_GET["action"]) {
      case "delivered":
          $ido = $_GET["order_id"];
          $sql="UPDATE orders SET status='delivered', delivered_at=NOW() WHERE order_id = '$ido'";
          $connection->query($sql);
          header('location:order_view.php');
      break;	

      case "received":
        //initialize value from get
        $ido = $_GET["order_id"];
        $msg = $_POST['msg'];
        $subject = "Order Number 000000".$ord_id;
        //end initialize


        $sql="UPDATE orders SET status='received', status=1 WHERE order_id = '$ido'";
        $connection->query($sql);

        $status_sql="INSERT INTO status(order_id, content)VALUES ('$ido','Order received!')";
        $connection->query($status_sql);

        // send notif to outlet
        $notif_sql = "INSERT INTO comments(send_to, comment_subject, order_id)VALUES ('$branch','Your order was received!', '$ido')";
        $connection->query($notif_sql);
        header('location:order_detail_view.php?order_id='.$ido);
      break;	

      case "processing":
        //initialize value from get
        $ido = $_GET["order_id"];
        $branch = $_GET["outlet"];
        //end initialize


        $sql="UPDATE orders SET status='In Transit', status=2 WHERE order_id = '$ido'";
        $connection->query($sql);

        $status_sql="INSERT INTO status(order_id, content)VALUES ('$ido','Order is being processed!')";
        $connection->query($status_sql);

        // send notif to outlet
        $notif_sql = "INSERT INTO comments(send_to, comment_subject, order_id)VALUES ('$branch','Order is being processed!', '$ido')";
        $connection->query($notif_sql);
        header('location:order_detail_view.php?order_id='.$ido);
      break;	

      case "in_transit":
          //initialize value from get
          $ido = $_GET["order_id"];
          $branch = $_GET["outlet"];
          //end initialize


          $sql="UPDATE orders SET status='In Transit', status=3 WHERE order_id = '$ido'";
          $connection->query($sql);

          $status_sql="INSERT INTO status(order_id, content)VALUES ('$ido','Your order was turned over to our driver and on its way!')";
          $connection->query($status_sql);

          // send notif to outlet
          $notif_sql = "INSERT INTO comments(send_to, comment_subject, order_id)VALUES ('$branch','Order is on its way!', '$ido')";
		      $connection->query($notif_sql);
          header('location:order_view.php');
      break;	

      case "delivery":
          //initialize value from get
          $ido = $_GET["order_id"];
          $branch = $_GET["outlet"];
          $notif_mes = ucfirst($branch)." has received their order.";
          //end initialize
          $sql="UPDATE orders SET status=4, status='delivered', delivered_at=NOW() WHERE order_id = '$ido'";
          $connection->query($sql);


          $status_sql="INSERT INTO status(order_id, content)VALUES ('$ido','$notif_mes')";
          $connection->query($status_sql);


          // send notif to commi
          $notif_sql = "INSERT INTO comments(send_to, comment_subject, order_id)VALUES ('commi','$notif_mes', '$ido')";
		      $connection->query($notif_sql);
          header('location:order_view.php');
      break;	
  }
  }

?>





        <!-- Order Status Details -->
        <?php
        while($row=$actord_res->fetch_array()){
          $edit_id = $row['order_id'];
          $customer_name = $row['firstname']." ".$row['lastname'];
          $email = $row['email'];
        }
        ?>
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Set Delivery Phase
          </div>
            <!-- 
            // 0 - Pending 
            // 1 - Received
            // 2 - Processing
            // 3 - In Transit
            // 4 - Delivered 
            -->
          <div class="card-body">
            <a href="order_detail_view.php?action=received&order_id=<?php echo $edit_id;?>" class="btn btn-primary">Receive Order</a>
            <a href="order_detail_view.php?action=processing&order_id=<?php echo $edit_id;?>" class="btn btn-primary">Process Order</a>
            <a href="order_detail_view.php?action=deliver&order_id=<?php echo $edit_id;?>" class="btn btn-primary">Deliver Order</a>
            <a href="order_detail_view.php?action=delivered&order_id=<?php echo $edit_id;?>" class="btn btn-primary">Delivered</a>
            <hr>
            <form method="POST">
              <input type="hidden" name="email" value="<?php echo $email; ?>">

              <div class="form-group">
                <label for="exampleFormControlTextarea1">Send a message to <?php echo $customer_name; ?>:</label>
                <textarea class="form-control" name="msg" id="editor1" rows="3"></textarea>
              </div>

              <input type="submit" class="btn btn-success" name="send" value="Send Message">
            </form>
          </div>
          <div class="card-footer small text-muted"> 
          </div>
        </div>



        <!-- Order Status Details-->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Order Status Details
          </div>
          <div class="card-body">
          <?php
          while($prow=$per_res->fetch_array()){ 
            // 0 - Pending 
            // 1 - Received
            // 2 - Processing
            // 3 - In Transit
            // 4 - Delivered

            //condition for status bar percentage
            $progress_percentage = 0; // value of progress bar
            $percentage_text = ''; // value of progress bar
            if($prow['status'] == 1){
              $progress_percentage = 25;
              $percentage_text = 'Received'; 
            } elseif($prow['status'] == 2){
              $progress_percentage = 50;
              $percentage_text = 'Processing'; 
            } elseif($prow['status'] == 3){
              $progress_percentage = 75;
              $percentage_text = 'In Transit'; 
            } elseif($prow['status'] == 0){
              $progress_percentage = 0;
              $percentage_text = 'Pending'; 
            } else {
              $progress_percentage = 100;
              $percentage_text = 'Delivered'; 
            }
          }
          ?>

          <div class="progress" style="height: 50px;">
            <div class="progress-bar progress-bar-animated progress-bar-striped  bg-success" role="progressbar" style="width: <?php echo $progress_percentage; ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?php echo $percentage_text; ?></div>
          </div>

          <?php
          //order details query
          while($srow=$order_status_res->fetch_array()){ 
          ?>

          <div class="card mt-3">
            <div class="card-header">
              <?php echo $srow['created_at'] ?>
            </div>
            <div class="card-body">
                <p><?php echo $srow['content'] ?></p>
            </div>
          </div>


          <?php }// end of while ?>
          </div>
          <div class="card-footer small text-muted"> 
          </div>
        </div>





        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Orders Details
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <!-- <button class="btn btn-primary btn-block btn-sm mb-3" data-toggle="modal" data-target="#add_order">Create Order</button> -->
              <table class="table table-bordered table-hover getid" id="dataTable_modal" width="100%" cellspacing="0">
          
                <thead>
                  <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Sub Total</th>
                  </tr>
                </thead> 
                <tbody>
                <!-- START PHP SCRIPT CATERGORY -->
                <?php
                //order details query
                while($drow=$o_details_result->fetch_array()){ 
                ?>
                  <tr>
                    <td><?php echo $drow['prod_name']; ?></td>
                    <td><?php echo number_format($drow['price'], 2); ?></td>
                    <td>
                    <?php 
                    // condition if qty is out of stock
                    if($drow['qty'] > 0){ 
                      //condition if qty is updated by commissary then show that value

                        echo $drow['qty']; 

                      // end condition if qty is updated by commissary
                    }
                    else{
                      echo "<label class='badge badge-danger text-wrap'> out of stock </label>";
                    }
                    // end condition for out of stock status
                    
                    ?>
                    </td>
                    <td> &#8369;
                      <?php
                          $subt = $drow['price']*$drow['qty'];
                          echo number_format($subt, 2);
                      ?>
                    </td>
                  </tr>
                <?php } ?>
                <!-- END PHP SCRIPT CATEGORY -->
                <?php 
                while($row=$order_result->fetch_array()){
                $edit_id = $row['order_id'];
                $stats = $row['status'];
                ?>
                <tr>
                    <td><b>TOTAL</b></td>
                    <td></td>
                        <!-- <td colspan="3"><b>TOTAL</b></td> -->
                      <td></td>
                      <td>&#8369; <?php echo number_format($row['total'], 2); ?></td>
                  </tr>
                  <tr>
                    <td> <b> Outlet: </b> </td>
                    <td> <?php echo ucfirst($row['firstname']); echo " ".ucfirst($row['lastname']); ?></td>
                        <!-- <td colspan="3"><b>TOTAL</b></td> -->
                      <td><b>Notes:</b></td>
                      <td> <?php echo $row['notes']; ?></td>
                  </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
          </div>
          <div class="card-footer small text-muted"> 
            <a href="order_view.php" class="btn btn-danger"><i class="fas fa-arrow-left">&nbsp;</i> Go Back</a>
          </div>
        </div>

        <?php include 'footer.php' ?>