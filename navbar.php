<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #063C75;">
  <a class="navbar-brand" href="#">Via Mare LOGO</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
    <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="delivery.php">Delivery</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vm.md" target="_blank">Website Current Sprint Status</a>
      </li>
    </ul>
  </div>
</nav>